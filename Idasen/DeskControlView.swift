// Copyright 2021 Kai Oezer

import SwiftUI
import DPG1

struct DeskControlView : View
{
	@EnvironmentObject var desk : DPG1
	@State var deskHeight : Int = Int(DPG1State.invalidPosition)

	private let _connection = DPG1Connection()

	var body: some View
	{
		VStack(spacing: 0)
		{
			_manualHeightControls()
			_presets()
			Spacer()
				.frame(height:8)
			StatusBar()
		}
		.frame(maxWidth:.infinity, maxHeight:.infinity)
		.environment(\.deskHeight, deskHeight)
		.onReceive(_connection.remoteStatePublisher.receive(on:RunLoop.main)) {
			desk.state = $0 // synchronizing local state with remote state
			deskHeight = Int(desk.state.position / 10) + Preferences.shared.deskHeightOffset
		}
		.onAppear {
			_connection.scanDevices()
		}
	}

	private func _manualHeightControls() -> some View
	{
		ZStack
		{
			HStack(spacing:0)
			{
				VStack
				{
					Spacer()
						.frame(height:40)
					Image("desk")
						.resizable()
						.antialiased(true)
						.aspectRatio(contentMode: .fit)
						.frame(width:160, height:160)
						.opacity(0.8)
				}
				Spacer()
			}
			VStack
			{
				Spacer()
					.frame(height:12)
				HStack(spacing:0)
				{
					Spacer()
						.frame(width:12)
					PositionIndicator(
						position: desk.state.position,
						showInInches: Preferences.shared.showPositionInInches)
					Spacer()
				}
				Spacer()
			}
			HStack
			{
				Spacer()
				VStack
				{
					Spacer()
						.frame(height:70)
					UpDownButton(
						upAction: {
							_connection.send(command:.moveUp)
							//print("move up")
						},
						downAction: {
							_connection.send(command:.moveDown)
							//print("move down")
						},
						releaseAction: {
							_connection.send(command: .stop)
							//print("stop")
						} )
						.frame(width:38, height:112)
					Spacer()
				}
				Spacer()
					.frame(width:20)
			}
		}
		.frame(maxWidth:.infinity, maxHeight:.infinity)
	}

	private func _presets() -> some View
	{
		HStack(spacing:0)
		{
			Spacer()
			PresetButton(presetPosition: Preferences.shared.$preset_1,
				action: { _sendMoveTo(Preferences.shared.preset_1) },
				startAction: { _sendStartMoveTo(Preferences.shared.preset_1) },
				endAction: { _connection.send(command: .stop) } )
				.frame(width:40, height:40)
			Spacer()
			PresetButton(presetPosition: Preferences.shared.$preset_2,
				action: { _sendMoveTo(Preferences.shared.preset_2) },
				startAction: { _sendStartMoveTo(Preferences.shared.preset_2) },
				endAction: { _connection.send(command: .stop) } )
				.frame(width:40, height:40)
			Spacer()
			PresetButton(presetPosition: Preferences.shared.$preset_3,
				action: { _sendMoveTo(Preferences.shared.preset_3) },
				startAction: { _sendStartMoveTo(Preferences.shared.preset_3) },
				endAction: { _connection.send(command: .stop) } )
				.frame(width:40, height:40)
			Spacer()
		}
		.padding(8)
		.frame(height: 50)
		.frame(maxWidth:.infinity)
	}

	private func _sendMoveTo(_ position : Int)
	{
		let dpg1Position = DPG1State.Position(position)
		//let byteFlippedPosition = (dpg1Position << 8) | (dpg1Position >> 8)
		withUnsafeBytes(of: dpg1Position) {
			let value = Data(bytes: $0.baseAddress!, count: $0.count)
			_connection.send(command: .moveTo, value: value)
		}
	}

	private func _sendStartMoveTo(_ targetPosition : Int)
	{
	#if true
		_connection.send(command: .prepMove)
	#else
		let currentPosition = Int(desk.state.position)
		if targetPosition > currentPosition
		{
			_connection.send(command: .moveUp)
		}
		else if targetPosition < currentPosition
		{
			_connection.send(command: .moveDown)
		}
	#endif
	}

}

// MARK: -

private struct deskHeightEnvironmentKey : EnvironmentKey
{
	static let defaultValue : Int = Int(DPG1State.invalidPosition)
}

extension EnvironmentValues
{
	var deskHeight : Int
	{
		get { self[deskHeightEnvironmentKey.self] }
		set { self[deskHeightEnvironmentKey.self] = newValue }
	}
}

// MARK: -

struct PositionIndicator : View
{
	let position : DPG1State.Position
	let showInInches : Bool
	@Environment(\.deskHeight) var deskHeight

	var body: some View
	{
		if position != DPG1State.invalidPosition
		{
			HStack(spacing:0)
			{
				if showInInches
				{
					let inches = deskHeight * 10 / 254 // 1 inch = 25,4 millimeters
					Text("\(inches / 12)′ \(inches % 12)″")
						.font(.custom("SF Pro", size:24))
						.foregroundColor(.white)
						.frame(width:56, height: 28, alignment: .trailing)
						.opacity(0.9)
					Spacer()
						.frame(width:2)
					let sixteenths = (deskHeight * 160 / 254) % 16
					Text("\(sixteenths)")
						.font(.custom("SF Mono", size:16))
						.foregroundColor(.white)
						.opacity(0.7)
						.frame(width:sixteenths > 9 ? 24 : 14, height:28, alignment: .trailing)
						.offset(x:0, y:-2)
					Text("/")
						.font(.custom("SF Mono", size:14))
						.foregroundColor(.white)
						.opacity(0.5)
						.frame(width:8, height:28, alignment: .leading)
						.offset(x:-3, y:2)
					Text("16")
						.font(.custom("SF Mono", size:14))
						.foregroundColor(.white)
						.opacity(0.5)
						.frame(width:24, height:28, alignment: .leading)
						.offset(x:-4, y:6)
				}
				else
				{
					Text("\(deskHeight / 10)")
						.font(.custom("SF Mono", size:24))
						.foregroundColor(.white)
						.opacity(0.9)
						.frame(width:52, height:28, alignment: .trailing)
					Text(".\(deskHeight % 10)")
						.font(.custom("SF Mono", size:18))
						.foregroundColor(.white)
						.opacity(0.5)
						.frame(width:24, height:28, alignment: .leading)
						.offset(x:0, y:2)
					Spacer()
						.frame(width:3)
					Text("cm")
						.font(.custom("SF Mono", size:24))
						.foregroundColor(.white)
						.opacity(0.5)
						.frame(width:60, height:28, alignment: .leading)
				}
			}
		}
	}
}

let _previewBackgroundColor = Color(red: 0.5, green: 0.5, blue: 0.8)

struct PositionIndicator_Preview : PreviewProvider
{
	static var previews : some View
	{
		let pos1 : DPG1State.Position = 783
		let pos2 : DPG1State.Position = 1210
		let pos3 : DPG1State.Position = 1100
		Group
		{
			PositionIndicator(position: pos1, showInInches: false)
			PositionIndicator(position: pos2, showInInches: false)
			PositionIndicator(position: pos1, showInInches: true)
			PositionIndicator(position: pos2, showInInches: true)
			PositionIndicator(position: pos3, showInInches: true)
		}
		.padding(10)
		.background(_previewBackgroundColor)
		.previewLayout(.fixed(width: 150, height: 40))
	}
}

// MARK: -

struct UpDownButton : View
{
	let upAction : ()->()
	let downAction : ()->()
	let releaseAction : ()->()

	var body : some View
	{
		ZStack
		{
			VStack
			{
				GeometryReader { geo in
					Path { path in
						path.move(to: CGPoint(x:0, y:geo.size.width/2))
						path.addLine(to: CGPoint(x:0, y:geo.size.height - geo.size.width/2))
						path.addArc(
							center: CGPoint(x:geo.size.width/2, y:geo.size.height - geo.size.width/2),
							radius: geo.size.width/2,
							startAngle: .degrees(180),
							endAngle: .zero,
							clockwise: true)
						path.addLine(to: CGPoint(x:geo.size.width, y:geo.size.width/2))
						path.addArc(
							center: CGPoint(x:geo.size.width/2, y:geo.size.width/2),
							radius: geo.size.width/2,
							startAngle: .zero,
							endAngle: .degrees(180),
							clockwise: true)
					}
					.fill(Color.white)
				}
			}
			VStack
			{
				Rectangle()
					.fill(Color.gray)
					.opacity(0.3)
					.frame(height:1)
					.frame(maxWidth:.infinity)
			}
			VStack
			{
				let arrowSize : CGFloat = 14
				let verticalInset : CGFloat = 10
				Spacer().frame(height:verticalInset)
				Image(systemName: "chevron.up")
					.resizable()
					.aspectRatio(contentMode: .fit)
					.foregroundColor(.black)
					.frame(width:arrowSize, height:arrowSize)
				Spacer()
				Image(systemName: "chevron.down")
					.resizable()
					.aspectRatio(contentMode: .fit)
					.foregroundColor(.black)
					.frame(width:arrowSize, height:arrowSize)
				Spacer().frame(height:verticalInset)
			}
			VStack(spacing:0)
			{
				Color.clear
					.contentShape(Rectangle())
					.modifier(DeskButtonLongPressGestureProvider(repeatAction: upAction, endAction: releaseAction))
					.help("move.up")
				Color.clear
					.contentShape(Rectangle())
					.modifier(DeskButtonLongPressGestureProvider(repeatAction: downAction, endAction: releaseAction))
					.help("move.down")
			}
		}
	}

}

struct UpDownButton_Preview : PreviewProvider
{
	static var previews : some View
	{
		UpDownButton(
			upAction: { print("up") },
			downAction: { print("down") },
			releaseAction: { print("released") })
			.padding(10)
			.background(_previewBackgroundColor)
			.previewLayout(.fixed(width: 70, height: 140))
	}
}

// MARK: -

/// Provides the gesture needed to trigger repeated actions while the mouse is pressed.
struct DeskButtonLongPressGestureProvider : ViewModifier
{
	let repeatAction : ()->()
	var startAction : (()->())?
	var endAction : (()->())?
	var waitDuration : Double? = nil
	var repeatStartDelay : TimeInterval? = nil

	@State private var _repeatTimer : Timer? = nil

	func body(content: Content) -> some View
	{
		content.gesture(
			LongPressGesture(minimumDuration: waitDuration ?? 0.1)
			.onEnded{ _ in
				startAction?()
				_repeatTimer = Timer(fire: Date(timeIntervalSinceNow: repeatStartDelay ?? 0), interval: 0.5, repeats: true) { _ in repeatAction() }
				RunLoop.main.add(_repeatTimer!, forMode: .default)
			}
			// The DragGesture is needed to activate the following TapGesture event if the mouse is dragged outside of the view.
			.simultaneously(with: DragGesture(minimumDistance: 1.0, coordinateSpace: .local))
			.onEnded { _ in }
			// The TapGesture is needed to send the final releaseAction.
			// LongPressGesture is finished after the minimum duration has passed.
			// Therefore, TapGesture also needs to clean up the timer.
			.sequenced(before: TapGesture())
			.onEnded { _ in
				assert(_repeatTimer != nil)
				_repeatTimer?.invalidate()
				_repeatTimer = nil
				endAction?()
			}
		)
	}
}

// MARK: -

struct PresetButton : View
{
	@Binding var presetPosition : Int
	let action : ()->()
	let startAction : ()->()
	let endAction : ()->()

	@EnvironmentObject var desk : DPG1
	@State var circleWidth : CGFloat = 1.0

	// Heuristically determined time to wait until the DPG1 unit has finished processing startAction.
	static let startDelay : TimeInterval = 1.0

	var body : some View
	{
		ZStack
		{
			if presetPosition == DPG1State.invalidPosition
			{
				Image(systemName:"star")
					.resizable()
					.frame(width:18, height:18)
					.foregroundColor(.white)
					.opacity(0.5)
			}
			else
			{
				Text(String((presetPosition/10 + Preferences.shared.deskHeightOffset)/10) )
					.bold()
					.foregroundColor(.white)
					.padding(6)
			}
			Circle()
				.strokeBorder(Color.white, lineWidth: circleWidth)
				.opacity(0.5)
				.padding(1)
		}
		.frame(maxWidth:.infinity, maxHeight:.infinity)
		//.gesture(TapGesture().modifiers(.option).onEnded{
		.gesture(TapGesture().onEnded{
			let withOptionKey = NSEvent.modifierFlags.contains(.option)
			let withShiftKey = NSEvent.modifierFlags.contains(.shift)
			if withOptionKey
			{
				presetPosition = Int(withShiftKey ? DPG1State.invalidPosition : desk.state.position)
				withAnimation(.easeOut(duration:0.2)) { circleWidth = 5.0 }
				DispatchQueue.main.async { withAnimation(.easeOut(duration:0.1)) { circleWidth = 1.0 } }
			}
		})
		.modifier(DeskButtonLongPressGestureProvider(repeatAction: action, startAction: startAction, endAction: endAction, repeatStartDelay: Self.startDelay))
		.contentShape(Rectangle())
		.help("preset-button.help")
	}
}

struct PresetButton_Previews : PreviewProvider
{
	static var previews : some View
	{
		Wrapper()
			.padding(10)
			.background(_previewBackgroundColor)
			.previewLayout(.fixed(width: 80, height: 60))
	}

	struct Wrapper : View
	{
		@State var position1 : Int = 8150
		@State var position2 : Int = Int(DPG1State.invalidPosition)

		var body : some View
		{
			Group
			{
				PresetButton(presetPosition: $position1, action: {}, startAction: {}, endAction: {})
				PresetButton(presetPosition: $position2, action: {}, startAction: {}, endAction: {})
			}
			.environmentObject(_desk(at: 10000))
		}

		private func _desk(at position : DPG1State.Position) -> DPG1
		{
			var state = DPG1State()
			state.position = position
			let desk = DPG1()
			desk.state = state
			return desk
		}
	}
}

// MARK: -

struct StatusBar : View
{
	@EnvironmentObject var desk : DPG1

	var body : some View
	{
		HStack(alignment:.center, spacing: 0)
		{
			Spacer()
				.frame(width:4)
			Image(systemName:(desk.state.connected ? "circle.fill" : "circle"))
				.resizable()
				.aspectRatio(contentMode: .fit)
				.foregroundColor(.white)
				.opacity(0.4)
				.frame(width:9, height:9)
			Spacer()
				.frame(width:6)
			Text(desk.state.name)
				.font(.custom("SF Pro Ultralight", size:12))
				.foregroundColor(.white)
				.opacity(0.5)
			Spacer()
		}
		.padding(4)
		.frame(height: 24)
		.frame(maxWidth: .infinity)
	}
}

struct StatusBar_Previews : PreviewProvider
{
	static var previews : some View
	{
		VStack()
		{
			StatusBar()
				.environmentObject(_desk(connected : true))
			StatusBar()
				.environmentObject(_desk(connected : false))
		}
		.background(_previewBackgroundColor)
		.previewLayout(.fixed(width: 200, height: 50))
	}

	static private func _desk(connected : Bool) -> DPG1
	{
		let desk = DPG1()
		desk.state = DPG1State(id: UUID(), name: "My Desk", position: 1100, ownerPresets: .none, guestPresets: .none, connected: connected)
		return desk
	}
}
