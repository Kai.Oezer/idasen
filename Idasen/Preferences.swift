//
//  Preferences.swift
//  Desk Controller
//
//  Created by David Williames on 11/1/21.
//
//  Modified 2021 by Kai Oezer

import SwiftUI
import DPG1

class Preferences
{
	private init() {}

	static let shared = Preferences()

	@AppStorage("preset_1")
	var preset_1 : Int = Int(DPG1State.invalidPosition)

	@AppStorage("preset_2")
	var preset_2 : Int = Int(DPG1State.invalidPosition)

	@AppStorage("preset_3")
	var preset_3 : Int = Int(DPG1State.invalidPosition)

	/// The height of the desk, in millimeters, when at its lowest position.
	///
	/// At this height, the position reported from the leg actuator should be zero.
	@AppStorage("positionOffsetValue")
	var deskHeightOffset : Int = 615

	@AppStorage("showPositionInInches")
	var showPositionInInches : Bool = !NSLocale.current.usesMetricSystem

	@AppStorage("didShowDisclaimer")
	var didShowDisclaimer : Bool = false
}
