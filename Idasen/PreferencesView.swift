//  Copyright 2021 Kai Oezer

import SwiftUI

struct PreferencesView : View
{
	@Environment(\.colorScheme) var colorScheme
	private let _appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String

	var body: some View
	{
		VStack(alignment:.leading, spacing: 0)
		{
			VStack(alignment:.leading, spacing: 10)
			{
				Toggle(isOn: Preferences.shared.$showPositionInInches)
				{
					Text("settings.height-in-inches")
				}

				HStack(spacing:6)
				{
					Text("settings.min-desk-height")
					TextField("", value: Preferences.shared.$deskHeightOffset, formatter: NumberFormatter())
						.frame(width:32)
						.multilineTextAlignment(.trailing)
						.accentColor(Color.blue)
						.foregroundColor(colorScheme == .dark ? Color.white : MainView.tint_dark)
					Text("mm")
				}
			}
			.foregroundColor(.white)
			.opacity(0.8)
			.padding(10)

			Spacer(minLength: 0)

			HStack(spacing: 0)
			{
				Spacer()
					.frame(width:8)
				Text("version \(_appVersion)")
					.font(.system(size:9))
					.foregroundColor(.white)
					.opacity(0.2)
				Spacer()
				Image(systemName:"xmark.circle.fill")
					.resizable()
					.aspectRatio(contentMode:.fit)
					.frame(width:16, height:16)
					//.foregroundStyle(.white, .red)
					.foregroundColor(.white)
					.background(Color.red)
					.mask(Circle())
					.opacity(1.0)
					.onTapGesture(perform: { NSApp.terminate(nil) })
					.help("quit-button")
				Spacer()
					.frame(width:2)
			}

			Spacer()
				.frame(height:2)
		}
		.padding(2)
		.frame(height: 100)
		.frame(minWidth: 100)
		.frame(maxWidth: .infinity)
		.background(Color(red:0, green:0, blue:0, opacity: 0.10))
	}

}

struct PreferencesView_Previews : PreviewProvider
{
	static var previews : some View
	{
		PreferencesView()
			.previewLayout(.fixed(width: 250, height: 120))
			.background(_previewBackgroundColor)
	}
}
