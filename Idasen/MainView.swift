//  Copyright 2021 Kai Oezer

import SwiftUI
import DPG1

struct MainView : View
{
	@State var showPreferences = false

	@State var showDisclaimer = !Preferences.shared.didShowDisclaimer

	@Environment(\.colorScheme) var colorScheme

	static let tint_light = Color(red: 0.483, green: 0.658, blue: 0.874)
	static let tint_dark  = Color(red: 0.145, green: 0.208, blue: 0.294)

	var body : some View
	{
		let controlWidth : CGFloat = 250
		let controlHeight : CGFloat = 300
		return VStack(spacing: 0)
		{
			ZStack(alignment:Alignment(horizontal: .leading, vertical: .top))
			{
				DeskControlView()
				if showDisclaimer
				{
					DisclaimerView {
						DispatchQueue.main.async { withAnimation(.easeOut(duration:0.2)) { showDisclaimer = false } }
						Preferences.shared.didShowDisclaimer = true
					}
				}
				_preferencesToggle()
			}
			.frame(width:controlWidth, height:controlHeight)
			if showPreferences
			{
				PreferencesView()
					.frame(width:controlWidth)
			}
		}
		.frame(maxWidth:.infinity)
		.background(_backgroundGradient)
	}

	struct PreferencesToggleStyle : ToggleStyle
	{
		func makeBody(configuration: Self.Configuration) -> some View
		{
			HStack
			{
				Image(systemName: configuration.isOn ? "gearshape.fill" : "gearshape")
					.resizable()
					.frame(width:16, height:16)
					.foregroundColor(.white)
					.opacity(configuration.isOn ? 1.0 : 0.6)
					.onTapGesture { configuration.isOn.toggle() }
			}
		}
	}

	private func _preferencesToggle() -> some View
	{
		HStack(spacing:0)
		{
			Spacer()
			VStack(spacing:0)
			{
				Spacer()
				Toggle( isOn: $showPreferences ) {}
					.toggleStyle(PreferencesToggleStyle())
					.help("settings.toggle.title")
				Spacer().frame(height:4)
			}
			Spacer().frame(width:4)
		}
	}

	private var _backgroundGradient : LinearGradient {
		let grad1 = Self.tint_light
		let grad2 = Color(red: 0.443, green: 0.608, blue: 0.824)
		let grad1Dark = Self.tint_dark
		let grad2Dark = Color(red: 0.125, green: 0.184, blue: 0.259)
		return LinearGradient(
			gradient: Gradient(colors: colorScheme == .dark ? [grad1Dark, grad2Dark] : [grad1, grad2]),
			startPoint: .top,
			endPoint: .bottom)
	}
}

struct MainView_Previews : PreviewProvider
{
	static var previews : some View
	{
		MainView(showPreferences: true)
			.previewLayout(.fixed(width: 250, height: 250))
	}
}
