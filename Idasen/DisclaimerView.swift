// Copyright 2021 Kai Oezer

import SwiftUI
import AppKit

struct DisclaimerView : View
{
	let acknowledgeAction : ()->()

	var body : some View
	{
		ZStack
		{
			BackgroundEffectView()

			VStack(spacing:20)
			{
				Text("disclaimer.title")
					.bold()
					.frame(alignment: .center)
					.frame(maxWidth:.infinity)
				Text("disclaimer.body")
					.font(.system(size: CGFloat(Float(NSLocalizedString("disclaimer.body.font-size", comment:"font size")) ?? 10)))
				Spacer()
				Button("disclaimer.button", action: acknowledgeAction)
					.buttonStyle(DisclaimerButtonStyle())
			}
			.cornerRadius(12)
			.padding(20)
		}
		.frame(maxWidth: .infinity, maxHeight: .infinity)
	}
}


fileprivate struct BackgroundEffectView: NSViewRepresentable
{
	func makeNSView(context: NSViewRepresentableContext<Self>) -> NSVisualEffectView
	{
		let view = NSVisualEffectView()
		view.material = .popover
		view.blendingMode = .withinWindow
		view.wantsLayer = true
		return view
	}

	func updateNSView(_ view: NSVisualEffectView, context: NSViewRepresentableContext<Self>)
	{
	}
}


fileprivate struct DisclaimerButtonStyle : ButtonStyle
{
	@Environment(\.colorScheme) var colorScheme

	func makeBody(configuration: Configuration) -> some View
	{
		configuration.label
			.font(.headline)
			.padding(10)
			.background(colorScheme == .dark ? Color.black : Color.blue)
			.foregroundColor(.white)
			.cornerRadius(6)
	}
}
