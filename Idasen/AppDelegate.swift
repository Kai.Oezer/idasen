//
//  AppDelegate.swift
//  Desk Controller
//
//  Created by David Williames on 10/1/21.
//
//  Modified 2021 by Kai Oezer.

import Cocoa
import SwiftUI
import DPG1

@main
class AppDelegate: NSObject, NSApplicationDelegate
{
	private var _desk = DPG1()

	private let _statusBarItem = NSStatusBar.system.statusItem(withLength: NSStatusItem.squareLength)
	private let _popover = NSPopover()
	private var _eventMonitor: EventMonitor?

	func applicationDidFinishLaunching(_ aNotification: Notification)
	{
		_setUpStatusBarItem()
		_setUpMainUI()
		_setUpEventMonitor()
	}

	@objc func clickedStatusItem(_ sender: NSStatusItem)
	{
		if _popover.isShown
		{
			_closePopover(sender)
		}
		else
		{
			_showPopover()
		}
	}

	private func _showPopover()
	{
		guard let button = _statusBarItem.button else { return }

		_popover.show(relativeTo: button.bounds, of: button, preferredEdge: NSRectEdge.minY)
		guard let window = _popover.contentViewController?.view.window else { return }
		window.becomeKey()
		_eventMonitor?.start()
	}

	private func _closePopover(_ sender: AnyObject?)
	{
		_popover.performClose(sender)
		_eventMonitor?.stop()
	}

	public static func bringToFront(window: NSWindow)
	{
		window.makeKeyAndOrderFront(nil)
		NSApp.activate(ignoringOtherApps: true)
	}

	private func _setUpStatusBarItem()
	{
		let statusBarMenu = NSMenu(title: "Idasen")
		statusBarMenu.addItem(.separator())

		if let button = _statusBarItem.button
		{
			if let image = NSImage(named: "StatusBarButtonImage")
			{
				image.size = NSSize(width: 16, height: 16)
				button.image = image
			}

			button.menu = statusBarMenu
			button.sendAction(on: [.leftMouseUp, .rightMouseUp])
			button.action = #selector(AppDelegate.clickedStatusItem(_:))
		}
	}

	private func _setUpMainUI()
	{
		_popover.behavior = .transient
		_popover.contentViewController = NSHostingController(rootView: MainView().environmentObject(_desk))
	}

	private func _setUpEventMonitor()
	{
		_eventMonitor = EventMonitor(mask: [.leftMouseDown, .rightMouseDown]) { [weak self] event in
			if let self = self, self._popover.isShown {
				self._closePopover(event)
			}
		}
		_eventMonitor?.start()
	}

}
