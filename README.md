# Idasen

Idasen is a macOS status bar app for controlling the IKEA<sup>&reg;</sup> Idåsen (pronounced 'e-doah-sen') desk.

<img style="width:50%; border-radius:12px" src="jekyll-source/images/screenshot1.png" alt="screenshot">

Idasen connects to Idåsen's LINAK<sup>&reg;</sup> DPG1C desk panel unit via Bluetooth to send commands that move the desk. These commands are not officially published by LINAK but have been reverse-engineered by others.

I did not have to start work on this Idasen controller app from scratch. My controller is based on
[David Williames' Desk Controller](https://github.com/DWilliames/idasen-controller-mac/tree/v1.0).
My changes to the user interface and the underlying code are very extensive, however. The result is a distinctly different implementation with a user interface that is closer to the [LINAK Desk Control app](https://www.linak.com/products/controls/desk-control-app).

Visit the [__product home page__](https://robo.fish/idasen) for a user-level description of the app.

