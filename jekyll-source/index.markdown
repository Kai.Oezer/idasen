---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: default
---

## Control your IKEA[^4] Id&aring;sen desk from the macOS status bar.

<img style="border-radius: 18px" src="images/screenshot1.png"/>

### Download 

<a href="https://gitlab.com/api/v4/projects/27198258/packages/generic/Idasen/{{ site.app_version }}/Idasen-{{ site.app_version }}.dmg" style="font-size:1.5em;font-weight:bold">v{{ site.app_version }}</a>   
<span style="font-size:0.8em;color:gray">for macOS 11 (Big Sur) or newer</span>

### Familiar look

With a user interface that adopts the look of the LINAK[^5] Desk Control iOS app[^2],
this app is the perfect macOS companion for controlling the height of your
IKEA[^4] Id&aring;sen desk without your phone.

<img style="border-radius: 18px" src="images/screenshot2.png"/>

### Personal presets

As in the LINAK app, you use the round buttons to manage your favorite positions.
What's different, is that these favorite positions are stored on your Mac, not on
the desk. Your favorite positions thus can not be changed by other users sharing the desk.
The favorite positions will not be transferred to the desk, however, and it will stop
at different positions when using the physical controller.

### Higher precision

As in the LINAK app[^2], the height of the desk is displayed in centimeters.
The added decimal place, however, gives you millimeter[^3] precision.

### Height calibration

As in the LINAK app[^2], you can calibrate the displayed position by measuring
the height from the floor to the desktop, and entering the value in the
Settings panel.

<img style="border-radius: 18px" src="images/screenshot3.png"/>

## Motivation

* __Providing an open-source alternative, in case the official LINAK app becomes unavailable or drops support.__  
Most people rely on LINAK's [Desk Control app](https://www.linak.com/products/controls/desk-control-app)
to set the preset positions of the Idåsen desk and to control the desk remotely.
However, your desk may outlive the availability or support of the LINAK app.
Open-source alternatives, like this app, make sure a fallback is available or can easily be created.  

  The sources of this app are available [__on GitLab__](https://gitlab.com/Kai.Oezer/idasen).
  The business-friendly license allows you to distribute custom commercial versions.

* __Creating a macOS application.__  
A macOS application eliminates the need to reach for your iOS or Android device, helping you stay focused on
your task. It makes sense to implement the app as a status bar item or a Notification Center widget,
given that the desk control application has very few functions (up/down and presets) and,
also for safety reasons, it should be located at a fixed position on the screen that is easily memorized
and reachable from any application context.

* __Adding features not available in the LINAK app.__  
For example, more preset positions for users who share a desk.

## Troubleshooting

* Make sure no other devices are currently connected to your desk.

* Make sure the Bluetooth name of your Idåsen desk starts with the word "Desk" followed by a space character and a number.

* Try resetting your desk:
  - Using the physical control on your desk, move the desk to its lowest position.
  - Push down the panel button as if trying to lower it even more until the desk
    makes a quick down and up motion.
  - Press and hold the Bluetooth button in the panel down until the blue light starts blinking.

## Copyright

Created by Kai Oezer.

Using components created by David Williames and Maxim Bilan.

<hr>

[^2]: Referring to version 2.0.7 of the [LINAK Desk Control app](https://www.linak.com/products/controls/desk-control-app/).

[^3]: When displaying the position in inches, the precision of the displayed position is one sixteenth of an inch.

[^4]: IKEA is a registered trademark of Inter IKEA Systems B.V.

[^5]: LINAK is a registered trademark of Linak A/S
